import requests
import time
class imageEmotion:
    def __init__(self, threshold=0.00001):

        self._url = 'https://api.projectoxford.ai/emotion/v1.0/recognize'
        self._key = 'ffac8973390c4308ba7ace9c3764ce1f'
        self._key2 = '35f8759fe8214f999049b65a02e63926'
        self._maxNumRetries = 10
        self.counter = 0

        self.headers = dict()
        self.headers['Ocp-Apim-Subscription-Key'] = self._key
        self.headers['Content-Type'] = 'application/octet-stream'

        self.headers2 = dict()
        self.headers2['Ocp-Apim-Subscription-Key'] = self._key2
        self.headers2['Content-Type'] = 'application/octet-stream'

        self.threshold = threshold

    def getEmotion(self, img_data):


        json = None
        params = None
        if (self.counter < 19):
            result = self.__processRequest__( json, img_data, self.headers, params )
        else:
            result = self.__processRequest__( json, img_data, self.headers2, params )

        self.counter += 1
        if (self.counter >= 38):
            self.counter = 0
        if result is not None:
            for currFace in result:
                emotions_Dictionary = currFace['scores']
                
        for key in emotions_Dictionary:
            if emotions_Dictionary[key]<self.threshold:
                emotions_Dictionary[key]=0.0  
            
        return emotions_Dictionary
            

    def __processRequest__( self,json, data, headers, params ):

        """
        Helper function to process the request to Project Oxford

        Parameters:
        json: Used when processing images from its URL. See API Documentation
        data: Used when processing image read from disk. See API Documentation
        headers: Used to pass the key information and the data type request
        """
        
        none_Dictionary=[{'scores':
                          {u'surprise': 0.0,
                           u'fear': 0.0,
                           u'disgust': 0.0,
                           u'contempt': 0.0,
                           u'anger': 0.0,
                           u'sadness': 0.0,
                           u'happiness': 0.0,
                           u'neutral': 0.0}}]

        
        retries = 0
        result = none_Dictionary

        while True:

            response = requests.request( 'post', self._url, json = json, data = data, headers = headers, params = params )

            if response.status_code == 429: 

                print( "Message1: %s" % ( response.json()['error']['message'] ) )
                print self.counter
                self.counter += 5
                if retries <= 10:
                    time.sleep(1) 
                    retries += 1
                    continue
                else: 
                    print( 'Error: failed after retrying!' )
                    break

            elif response.status_code == 200 or response.status_code == 201:

                if 'content-length' in response.headers and int(response.headers['content-length']) == 0: 
                    result = none_Dictionary 
                elif 'content-type' in response.headers and isinstance(response.headers['content-type'], str): 
                    if 'application/json' in response.headers['content-type'].lower():
                        result = response.json() if response.content else none_Dictionary
                        if (result == []): result = none_Dictionary
                    elif 'image' in response.headers['content-type'].lower():
                        result = response.content
            else:
                try:
                    pass
                except:
                    print( "Error code: %d" % ( response.status_code ) )
                    print( "Message2: %s" % ( response.json()['error']['message'] ) )

            break
            
        return result






 
 
 

