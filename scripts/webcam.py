import time
from SimpleCV import Camera
import base64
class WebCam:
    def __init__(self, width=640, height=480, shutterspeed=0.01):
       self.width = width
       self.height = height
       self.shutterspeed = shutterspeed


    def captureImage(self):
      # cam = Camera()
        time.sleep(self.shutterspeed)  # If you don't wait, the image will be dark
        myusbcam = Camera(1, {"width": self.width, "height": self.height})
      # data = myusbcam.getImage()
        myusbcam.getImage().save("nicepicture.jpg")

        time.sleep(0.01)
        return r'nicepicture.jpg'