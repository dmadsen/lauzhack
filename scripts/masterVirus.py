import requests
import datetime

import json
from heartbeat import  PulseEmulator
from webcam import WebCam
import time
from emotion import imageEmotion
from keyboard import KeyBoard
#from GUI import imageGui
sampletime = 0
def main():

    # Initialize PulseEmulator and start it
    hb = PulseEmulator(basePulse=72, dbg=False)
    hb.run()
    wc = WebCam()
    ie = imageEmotion()
    kb = KeyBoard()
    kb.run()
    #gui = imageGui()
    #gui.run()
    #sr = speechRecog()
    #sr.run()
    while True:
        # The big main looooooop

        #print hb.getPulse()

        pathToFileInDisk = wc.captureImage()
        with open(pathToFileInDisk, 'rb') as f:
            data = f.read()
        emotions = ie.getEmotion(data)
        print emotions
        kb.update(emotions)
        time.sleep(sampletime)

        ############     #for rufus#############
        #url = 'http://192.33.205.5:8000/wsm/default/log_emotion'
        url = 'http://127.0.0.1:8000/wsm/default/log_emotion'

        dict = {}
        locTime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        emotions["time"] = locTime
        dict['image'] = emotions
        dict['heartbeat'] = {"hb":hb.getPulse(), "time":locTime}
        #dict['speech'] = {"text":"hi denise", "time":locTime}
        payload = dict
        # GET
        #r = requests.get(url)
        # GET with params in URL
        #r = requests.get(url, params=payload)
        # POST with form-encoded data
        #r = requests.post(url, data=payload)
        # POST with JSON
        #requests.post(url, data=json.dumps(payload))
        r = requests.post(url, data=json.dumps(payload))
        # Response, status etc
        #r.text
        #r.status_code
        ####################      #for rufus#############

    hb.close()


if __name__ == '__main__':
    import sys
    print(sys.version)
    main()
