# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations
import threading

lock = threading.Lock()
# -------------------------------------------------------------------------
# This is a sample controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# -------------------------------------------------------------------------
def liveEmotion():
    import json

    username = 'slava'
    #emo = db(db.emotions.username==username).select(orderby=~db.emotions.id).first()

    stat = db(db.livestats.username == username).select().first()
    #return dict(usernames=rows, emostats=liveemostats)
    retDic = {"surprise": stat["surprise"],
              "anger": stat["anger"],
              "neutral": stat["neutral"],
              "happiness": stat["happiness"],
              "disgust": stat["disgust"],
              "fear": stat["fear"],
              "sadness": stat["sadness"],
              "contempt": stat["contempt"]
              }
    return json.dumps(retDic)

import datetime
def tableLive():
    # s = py.Stream(stream_id1)
    # s.open()
    # s.write(dict(x=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f'), y=90))
    # s.close()
    pass

def updateTableData():
    #__updateTableData__()
    pass


def __updateTableData__():
    import json

    maxItems = 50
    username = 'slava'

    emo = db(db.emotions.username==username).select(orderby=~db.emotions.id, limitby=(0, maxItems))
    time = []
    neutral = []
    sadness = []
    happiness = []
    disgust = []
    anger = []
    surprise = []
    fear = []
    contempt = []

    for item in emo:

        time.append(item.logtime)
        neutral.append(item.neutral)
        sadness.append(item.sadness)
        happiness.append(item.happiness)
        disgust.append(item.disgust)
        anger.append(item.anger)
        surprise.append(item.surprise)
        fear.append(item.fear)
        contempt.append(item.contempt)

    trace0 = Scatter(
        x=time,
        y=neutral,
        name='neutral',
        line=dict(
            color='gray'
        )
    )
    trace1 = Scatter(
        x=time,
        y=sadness,
        name='sadness',
        line=dict(
            color='blue'
        )
    )
    trace2 = Scatter(
        x=time,
        y=happiness,
        name='happiness',
        line=dict(
            color='yellow'
        )
    )
    trace3 = Scatter(
        x=time,
        y=disgust,
        name='disgust',
        line=dict(
            color='green'
        )
    )
    trace4 = Scatter(
        x=time,
        y=anger,
        name='anger',
        line=dict(
            color='red'
        )
    )
    trace5 = Scatter(
        x=time,
        y=surprise,
        name='surprise',
        line=dict(
            color='purple'
        )
    )
    trace6 = Scatter(
        x=time,
        y=fear,
        name='fear',
        line=dict(
            color='black'
        )
    )
    trace7 = Scatter(
        x=time,
        y=contempt,
        name='contempt',
        line=dict(
            color='beige'
        )
    )
    data = Data([trace0, trace1, trace2, trace3, trace4, trace5, trace6, trace7])
    print "LALALA", data
    py.plot(data, filename='emotions', auto_open=False)


    emo = db(db.heartbeat.username==username).select(orderby=~db.heartbeat.id, limitby=(0, maxItems))
    time = []
    hb = []
    for item in emo:
        time.append(item.logtime)
        hb.append(item.heartbeat)

    trace0 = Scatter(
        x=time,
        y=hb,
        name='heartbeat',
    )

    data = Data([trace0])

    py.plot(data, filename='heartbeat', auto_open=False)

    return json.dumps({'status': 'OK'})


def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    updateTableData()
    return dict(message=T('Welcome to Gaming with eMotions!'))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()

def livestats():
    rows = db(db.livestats.username).select()
    print rows
    print request.vars
    updateTableData()
    if request.vars.username:
        print "here!"
        liveemostats = db(db.livestats.username == request.vars.username).select().first()
        return dict(usernames=rows, emostats=liveemostats)

    return dict(usernames=rows, emostats="")


def allstats():
    rows = db(db.livestats.username).select()
    print rows
    print request.vars
    updateTableData()
    if request.vars.username:
        print "here!"
        liveemostats = db(db.livestats.username == request.vars.username).select().first()
        # liveemostats = db(db.livestats.username == request.vars.username).select() # Commented due to issues with Plot
        return dict(usernames=rows, emostats=liveemostats)

    return dict(usernames=rows, emostats="")

def log_emotion():
    import json
    stuff = None
    for x in request.vars:
        stuff = json.loads(x)
    try:
        dbinsert = True
        username = 'slava'
        game = 'DOTA'
        milestone = 'level 1'
        session = 0
        try:
            hb = stuff['heartbeat']
            print hb
            # s = py.Stream(stream_id1)
            # s.open()
            # s.write(dict(x=hb['time'], y=hb['heartbeat']))
            # s.close()
            if dbinsert:
                db.heartbeat.insert(logtime=hb['time'],
                                    username=username,
                                    game=game,
                                    milestone=milestone,
                                    sess=session,
                                    heartbeat=hb['hb']
                                    )
                db.commit
        except:
            pass
        try:
            im = stuff['image']
            print im
            if dbinsert:
                db.emotions.insert(logtime=im['time'],
                                   username=username,
                                   game=game,
                                   milestone=milestone,
                                   sess=session,
                                   neutral=im['neutral'],
                                   sadness=im['sadness'],
                                   happiness=im['happiness'],
                                   disgust=im['disgust'],
                                   anger=im['anger'],
                                   surprise=im['surprise'],
                                   fear=im['fear'],
                                   contempt=im['contempt'],
                                   )
                db.commit
        except:
            pass
        try:
            tx = stuff['text']
            print tx
            if dbinsert:
                db.textreading.insert(logtime=tx['time'],
                                      username=username,
                                      game=game,
                                      milestone=milestone,
                                      sess=session,
                                      textinput=tx['text'])
                db.commit
        except:
            pass

        try:
            imImg = stuff['image']
            imHb = stuff['heartbeat']
            print dbinsert
            if dbinsert:
                # db.livestats.update_or_insert(db.livestats.username == username,
                #                     username=username,
                #                    logtime=imImg['time'],
                #                    game="game",
                #                    milestone=milestone,
                #                    sess=session,
                #                    neutral=imImg['neutral'],
                #                    sadness=imImg['sadness'],
                #                    happiness=imImg['happiness'],
                #                    disgust=imImg['disgust'],
                #                    anger=imImg['anger'],
                #                    surprise=imImg['surprise'],
                #                    fear=imImg['fear'],
                #                    contempt=imImg['contempt'],
                #                     heartbeat=imHb['hb'],
                #                    )
                db(db.livestats.username == username).delete()
                db.livestats.insert(username=username,
                                    logtime=im['time'],
                                   game=game,
                                   milestone=milestone,
                                   sess=session,
                                   neutral=im['neutral'],
                                   sadness=im['sadness'],
                                   happiness=im['happiness'],
                                   disgust=im['disgust'],
                                   anger=im['anger'],
                                   surprise=im['surprise'],
                                   fear=im['fear'],
                                   contempt=im['contempt'],
                                   )
                db.commit
        except:
            pass

    except:
        pass


    return dict()